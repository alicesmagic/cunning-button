import javax.swing.*;

/**
 * Created by Alice on 3:15 26.09.2019.
 */
public class MainApplication {
    public static void main(String[] args) {
        // Создать фрейм в потоке диспетчеризации событий
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new CunningButton();
            }
        });
    }
}