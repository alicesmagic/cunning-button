import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Alice on 18:12 26.09.2019.
 */
public class CunningButton implements MouseListener {
    private JButton jbtnClick;

    CunningButton() {
        JFrame jfrm = new JFrame("Cunning Button");
        jfrm.setLayout(null);
        jfrm.setResizable(false);
        jfrm.setSize(400, 400);
        jfrm.setLocationRelativeTo(null);
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jbtnClick = new JButton("Click Me");
        jbtnClick.setBounds(150, 180, 100, 30);
        jbtnClick.setCursor
                (Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        jbtnClick.setFocusPainted(false);
        jbtnClick.addMouseListener(this);
        jfrm.add(jbtnClick);
        jfrm.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (jbtnClick.getText().equals("Click Me")) {
            jbtnClick.setText("You Win!");
            jbtnClick.setBackground(new Color(60,240,90));
        }
        else {
            jbtnClick.setText("Click Me");
            jbtnClick.setBackground(null);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (jbtnClick.getText().equals("Click Me")) {
            jbtnClick.setLocation((int) (Math.random() * 301),
                    (int) (Math.random() * 351));
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

}